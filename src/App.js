import logo from './logo.svg';
import './App.css';
import Navbar from './BaiTapThucHanhLayout/Navbar';
import Banner from './BaiTapThucHanhLayout/Banner';
import Item from './BaiTapThucHanhLayout/Item';
import Footer from './BaiTapThucHanhLayout/Footer';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Banner />
      <Item />
      <Footer />
    </div>
  );
}

export default App;
